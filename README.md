#wechat-sdk

[#wechat-mp](http://git.oschina.net/juapk/wechat-mp)

> 技术讨论 QQ群：492238239

>[http://www.oschina.net/p/wechat-java-sdk-juapk](http://www.oschina.net/p/wechat-java-sdk-juapk)

> Maven 坐标
```
<dependency>
  <groupId>com.baomidou</groupId>
  <artifactId>wechat-sdk</artifactId>
  <version>1.b.0</version>
</dependency>
```

> 使用说明
```
/**
 * 微信测试
 * <p>
 * 访问地址： http://域名/test.html ，注意！请使用 80 端口。
 * </p>
 */
@Controller
public class WechatController extends BaseController {
	/**
	 * 测试
	 * <p>
	 * 登录地址： https://mp.weixin.qq.com 获取 appId, appSecret，或者申请测试账号地址：
	 * http://w.url.cn/s/AbQRrA6
	 * </p>
	 */
	@ResponseBody
	@RequestMapping("/test")
	public void test() {
		try {
			MPAccount mpAct = new MPAccount();
			mpAct.setAppId("wx54cc58197a655832");
			mpAct.setAppSecret("d4624c36b6795d1d99dcf0547af5443d");
			/**
			 * 必须为英文或数字，长度为3-32字符。 与公众号 Token 保持一致
			 * <p>
			 * 随机生成 32 位 Token 执行: MPAccount.issueToken()
			 * </p>
			 */
			mpAct.setToken("6e0bbcecc596cf8ab91484e3c3cd9d98");
			new WechatBootstrap(request, response, mpAct, new TuringHandler()).startup();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
```


关注我
====================
![程序员日记](http://git.oschina.net/uploads/images/2016/0121/093728_1bc1658f_12260.png "程序员日记")

