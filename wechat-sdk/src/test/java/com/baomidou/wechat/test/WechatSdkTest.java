package com.baomidou.wechat.test;

import org.junit.Assert;
import org.junit.Test;

import com.baomidou.wechat.WechatSdk;
import com.baomidou.wechat.vo.MPAccount;

public class WechatSdkTest {

	/**
	 * 版本号
	 */
	@Test
	public void version() {
		Assert.assertEquals("1.b.0", WechatSdk.version());
	}
	
	/**
	 * 32位 Token
	 */
	@Test
	public void issueToken() {
		Assert.assertEquals(32, MPAccount.issueToken().length());
	}
}
