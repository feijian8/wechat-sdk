package com.baomidou.wechat.test;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.wechat.api.WechatAPIImpl;
import com.baomidou.wechat.vo.MPAccount;
import com.baomidou.wechat.vo.api.Menu;
import com.baomidou.wechat.vo.api.MenuButton;
import com.baomidou.wechat.vo.api.QRTicket;

public class ApiTest {

	public static void main(String[] args) {
		MPAccount ma = new MPAccount();
		ma.setAppId("你的 appID");
		ma.setAppSecret("你的 appSecret");
		ma.setMpId("10001212");
		WechatAPIImpl wa = new WechatAPIImpl(ma, new WechatCacheMap());
		System.out.println(wa.getAccessToken());
		List<String> ipList = wa.getServerIps();
		System.out.println((ipList == null) ? "not get ip!" : ipList.get(0));

		System.out.println(wa.getFollowerList("").getOpenIds().toString());

		System.out.println(wa.getFollower("owvMfwSh9FbWDyW3ihBRdgK65e_g", "zh_CN").toString());
		QRTicket qt = wa.createQR(0, 123);
		System.out.println(qt != null ? qt.getTicket() : "qt is null");

		String longUrl = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=gQGt7zoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL09qb0lWVFhsUjVnX2JOTEgwQlFHAAIEa/Q2VgMEewAAAA==";
		System.out.println("shortUrl: " + wa.getShortUrl(longUrl));
		
		
		wa.getMenu();
		System.out.println("删除菜单：" + wa.delMenu());
		
		List<Menu> button = new ArrayList<Menu>();
		Menu m = new Menu();
		m.setType("click");
		m.setName("今日歌曲");
		m.setKey("V1001_TODAY_MUSIC");
		button.add(m);
		Menu m1 = new Menu("菜单");
		List<Menu> sub_button = new ArrayList<Menu>();
		sub_button.add(new Menu("IP归属地查询", "view", "http://wechat.eicp.net/ip/search.html"));
		sub_button.add(new Menu("视频", "view", "http://v.qq.com/"));
		sub_button.add(new Menu("赞一下我们", "click", "V1001_GOOD"));
		m1.setSub_button(sub_button);
		button.add(m1);
		MenuButton btn = new MenuButton();
		btn.setButton(button);
		System.out.println("菜单创建：" + wa.createMenu(btn));
		
		//删除菜单
		//wa.delMenu();
		
		// File file = new File("D://bg_link.jpg");
		// Media media = wa.upMedia(MediaType.IMAGE, file);
		// if (media != null && media.isOK()) {
		// System.out.println(media.getMedia_id());
		// }

		// String
		// mid="g0pW6j9P3QxY9DqBvmOuk85BU-NX_T97XnGhmh85jZUJnwhAs0CsuN1IgrlBOyXU";
		// Attachment att = wa.dlMedia(mid);
		// StreamUtil.outputFile(att.getFileStream(), new
		// File("D://bg_link10.jpg"));
	}

}
