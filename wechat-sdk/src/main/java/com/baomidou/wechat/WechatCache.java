package com.baomidou.wechat;

/**
 * 本地缓存接口设计
 * <p>
 * 用于存储会话数据(主要是access_token,jsticket)
 * </p>
 */
public abstract class WechatCache {

	public abstract Object get(String key);

	public abstract void set(String key, Object object, long expires);

	public abstract void remove(String key);
}
