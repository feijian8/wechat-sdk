package com.baomidou.wechat.exception;

/**
 * 微信高级API请求异常
 */
public class WechatException extends Exception {

	private static final long serialVersionUID = -303278319021435258L;

	public WechatException() {
		super();
	}

	public WechatException(String message, Throwable cause) {
		super(message, cause);
	}

	public WechatException(String message) {
		super(message);
	}

	public WechatException(Throwable cause) {
		super(cause);
	}

}
