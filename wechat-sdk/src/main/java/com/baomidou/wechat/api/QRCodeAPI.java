package com.baomidou.wechat.api;

import com.baomidou.wechat.vo.api.QRTicket;

/**
 * 微信二维码接口
 * <p>
 * http://mp.weixin.qq.com/wiki/18/28fc21e7ed87bec960651f0ce873ef8a.html
 * </p>
 * <p>
 * 下载二维码地址
 * HTTP GET请求（请使用https协议）
 * https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET
 * 提醒：TICKET记得进行UrlEncode
 * </p>
 */
public interface QRCodeAPI {

    /**
     * 创建二维码ticket地址
     */
    static final String API_CREATE_QRCODE = "/qrcode/create?access_token=%s";

    /**
     * 创建二维码ticket
     * 
     * @param sceneId
     *            场景Id(大于0 表示 临时码,Number 表示永远二维码, String 表示永远字符串)
     * @param expireSeconds
     *            二维码有效时间，以秒为单位。 最大不超过604800(即7天)
     * @return
     */
    QRTicket createQR(Object sceneId, int expireSeconds);
    
}
