/**
 * 微信公众平台所有的API
 * <p>
 * 开发者文档：http://mp.weixin.qq.com/wiki/11/0e4b294685f817b95cbed85ba5e82b8f.html
 * </p>
 */
package com.baomidou.wechat.api;