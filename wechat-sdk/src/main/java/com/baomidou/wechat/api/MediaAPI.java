package com.baomidou.wechat.api;

import java.io.File;

import com.baomidou.wechat.common.enums.MediaType;
import com.baomidou.wechat.common.util.Attachment;
import com.baomidou.wechat.vo.api.Media;

/**
 * 微信多媒体数据接口
 */
public interface MediaAPI {
	/**
	 * 上传多媒体
	 */
	static final String API_UPLOAD_MEDIA = "/media/upload?access_token=%s&type=%s";

	/**
	 * 下载多媒体
	 */
	static final String API_GET_MEDIA = "/media/get?access_token=%s&media_id=%s";

	/**
	 * 上传多媒体文件
	 * 
	 * <pre/>
	 * 上传的临时多媒体文件有格式和大小限制,如下:
	 * <li/>图片(image): 1M,支持JPG格式
	 * <li/>语音(voice):2M,播放长度不超过60s,支持AMR\MP3格式
	 * <li/>视频(video):10MB,支持MP4格式
	 * <li/>缩略图(thumb):64KB,支持JPG格式
	 * 
	 * <pre/>
	 * 媒体文件在后台保存时间为3天,即3天后media_id失效。
	 * 
	 * @param mediaType
	 *            多媒体类型 {@link com.baomidou.wechat.common.enums.MediaType}
	 * @param mediaFile
	 *            多媒体文件
	 * @return 实体{@link Media}
	 */
	Media upMedia(MediaType mediaType, File mediaFile);

	/**
	 * 下载多媒体文件
	 * 
	 * @param mediaId
	 *            媒体文件ID
	 * @return {@link Attachment}
	 */
	Attachment dlMedia(String mediaId);
}
