package com.baomidou.wechat.api;

import java.util.List;

import com.baomidou.wechat.vo.api.Follower;
import com.baomidou.wechat.vo.api.FollowerList;

/**
 * 微信用户信息接口
 */
public interface UserAPI {
	/**
	 * 用户列表地址
	 */
	static final String API_USER_LIST = "/user/get?access_token=%s&next_openid=%s";

	/**
	 * 用户基本信息地址
	 */
	static final String API_USER_INFO = "/user/info?access_token=%s&openid=%s&lang=%s";

	/**
	 * 批量用户基本信息地址
	 */
	static final String API_BATCH_USER_INFO = "/user/info/batchget?access_token=%s";

	/**
	 * 设置备注名地址
	 */
	static final String API_USER_REMARK = "/user/info/updateremark?access_token=%s";

	/**
	 * 设置用户备注名
	 * 
	 * @param openId
	 *            用户标识
	 * @param remark
	 *            新的备注名,长度必须小于30字符
	 * @return true 或 false
	 */
	boolean updateRemark(String openId, String remark);

	/**
	 * 获取用户基本信息(包括UnionID机制)
	 * 
	 * @param openId
	 *            用户的标识
	 * @param lang
	 *            国家地区语言版本,zh_CN 简体,zh_TW 繁体,en 英语
	 * @return 关注用户{@link Follower}
	 */
	Follower getFollower(String openId, String lang);

	/**
	 * 获取关注用户列表
	 * 
	 * @param nextOpenId
	 *            第一个拉取的OPENID,不填默认从头开始拉取
	 * @return 关注列表{@link FollowerList}
	 */
	FollowerList getFollowerList(String nextOpenId);

	/**
	 * 批量获取用户基本信息[最多拉取100条]
	 * 
	 * @return 关注用户{@link Follower}
	 */
	List<Follower> getFollowers();
}
