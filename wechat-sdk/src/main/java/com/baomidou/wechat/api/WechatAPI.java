package com.baomidou.wechat.api;

/**
 * 微信公众平台所有API汇集
 */
public interface WechatAPI extends CredentialAPI, UserAPI, MessageAPI, MediaAPI, QRCodeAPI, MenuAPI {

	/**
	 * 微信公众平台API入口
	 */
	static final String API_WECHAT = "https://api.weixin.qq.com/cgi-bin";
}
