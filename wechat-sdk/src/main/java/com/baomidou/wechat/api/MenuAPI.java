package com.baomidou.wechat.api;

import java.util.List;

import com.baomidou.wechat.vo.api.Menu;
import com.baomidou.wechat.vo.api.MenuButton;

/**
 * 微信自定义菜单接口
 */
public interface MenuAPI {
	/**
	 * 菜单查询地址
	 */
	static final String API_QUERY_MENU = "/menu/get?access_token=%s";
	/**
	 * 菜单创建地址
	 */
	static final String API_CREATE_MENU = "/menu/create?access_token=%s";
	/**
	 * 菜单删除地址
	 */
	static final String API_DEL_MENU = "/menu/delete?access_token=%s";

	/**
	 * 查询当前自定菜单
	 * 
	 * @return 菜单项{@link com.baomidou.wechat.vo.api.Menu}
	 */
	List<Menu> getMenu();

	/**
	 * 创建自定义菜单按钮
	 * 
	 * @param menuButton
	 *            菜单按钮
	 * @return true 或 false
	 */
	boolean createMenu(MenuButton menuButton);

	/**
	 * 删除自定义菜单
	 * <p>
	 * 该接口存在缓存，不能及时看到效果，退出微信从新登录生效。
	 * </p>
	 * 
	 * @return true 或 false
	 */
	boolean delMenu();
}
