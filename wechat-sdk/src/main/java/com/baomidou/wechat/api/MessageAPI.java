package com.baomidou.wechat.api;

import com.baomidou.wechat.vo.api.Template;

/**
 * 微信高级消息接口
 */
public interface MessageAPI {
	/**
	 * 发送客服消息地址
	 */
	static final String API_SEND_CUSTOM = "/message/custom/send?access_token=%s";
    /**
     * 发模板消息地址
     */
    static final String API_SEND_TEMPLATE = "/message/template/send?access_token=%s";

    /**
     * 发送模板消息
     * 
     * @param openId
     *            接收用户Id
     * @param tmlId
     *            模板Id
     * @param topColor
     *            顶部颜色
     * @param url
     *            跳转链接
     * @param tmls
     *            模板数据
     * @return 消息Id
     */
    long sendTemplateMsg(String openId, String tmlId, String topColor, String url, Template... tmls);
}
