package com.baomidou.wechat.api;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Connection.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.baomidou.wechat.common.util.Attachment;
import com.baomidou.wechat.common.util.HttpUtil;
import com.baomidou.wechat.exception.WechatException;
import com.baomidou.wechat.vo.api.Media;

/**
 * 微信平台支持
 */
public class WechatAPISupport {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 组合请求地址
	 */
	protected String mergeUrl(String url, Object... values) {
		StringBuffer uri = new StringBuffer(WechatAPI.API_WECHAT);
		if (values != null) {
			uri.append(String.format(url, values));
		}
		return uri.toString();
	}

	/**
	 * Http Get 请求
	 * 
	 * @param clazz
	 *            返回对象
	 * @param url
	 *            请求地址
	 * @param values
	 *            请求地址参数值
	 * @return 返回对象{@link T}
	 */
	protected <T> T httpGet(Class<T> clazz, String url, Object... values) {
		try {
			Response resp = HttpUtil.get(mergeUrl(url, values));
			if (isOK(resp.statusCode())) {
				return (T) JSON.parseObject(resp.body(), clazz);
			}
		} catch (Exception e) {
			logger.error(" httpGet error.", e);
		}
		return null;
	}

	/**
	 * Http Post 请求
	 * 
	 * @param clazz
	 *            返回对象
	 * @param data
	 *            请求参数
	 * @param url
	 *            请求地址
	 * @param values
	 *            请求地址参数值
	 * @return 微信 API 返回结果{@link T}
	 */
	protected <T> T httpPost(Class<T> clazz, Map<String, String> data, String url, Object... values) {
		try {
			Response resp = HttpUtil.post(mergeUrl(url, values), data);
			if (isOK(resp.statusCode())) {
				return (T) JSON.parseObject(resp.body(), clazz);
			}
		} catch (Exception e) {
			logger.error(" httpPost error.", e);
		}
		return null;
	}

	/**
	 * 上传媒体文件
	 * 
	 * @param file
	 *            文件
	 * @param url
	 *            请求参数
	 * @param values
	 *            请求地址参数值
	 * @return 微信 API 返回结果{@link Media}
	 */
	protected Media httpPostFile(File file, String url, Object... values) {
		try {
			String retJson = HttpUtil.upload(mergeUrl(url, values), file);
			if (StringUtils.isNotBlank(retJson)) {
				return JSON.parseObject(retJson, Media.class);
			}
			// FileInputStream fis = new FileInputStream(file);
			// Response resp = HttpHelper.getConnection(mergeUrl(url,
			// values)).data("media", file.getName(), fis)
			// .header("Content-Type",
			// "multipart/form-data").method(Method.POST).execute();
			// if (isOK(resp.statusCode())) {
			// String retJson = resp.body();
			// }
		} catch (Exception e) {
			logger.error(" httpPostFile error.", e);
		}
		return null;
	}

	/**
	 * HTTP POST 请求
	 * 
	 * @param clazz
	 *            返回对象
	 * @param json
	 *            请求json
	 * @param url
	 *            请求地址
	 * @param values
	 *            请求地址参数值
	 * @return 微信 API 返回结果{@link T}
	 */
	public <T> T httpPostJson(Class<T> clazz, String json, String url, Object... values) {
		try {
			String retJson = HttpUtil.postJson(mergeUrl(url, values), json);
			if (StringUtils.isNotBlank(retJson)) {
				return JSON.parseObject(retJson, clazz);
			}
		} catch (Exception e) {
			logger.error(" httpPostFile error.", e);
		}
		return null;
	}

	/**
	 * 下载媒体文件
	 * 
	 * @param url
	 *            请求参数
	 * @param values
	 *            请求地址参数值
	 * @return 下载媒体文件 返回结果{@link Attachment}
	 */
	public Attachment httpDownload(String url, Object... values) {
		try {
			return HttpUtil.download(mergeUrl(url, values));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 判断HTTP请求是否成功
	 * 
	 * @param statusCode
	 *            HTTP请求返回状态码
	 * @return
	 * @throws WechatException
	 */
	protected boolean isOK(int statusCode) throws WechatException {
		if (statusCode == 200) {
			return true;
		} else if (statusCode >= 500 && statusCode < 600) {
			logger.error("http server error.");
		} else if (statusCode >= 400 && statusCode < 500) {
			logger.error("http client error.");
		}
		return false;
	}
}
