package com.baomidou.wechat;

/**
 * 项目版本号声明
 * 
 * <pre/>
 * (参考:http://nutzam.com/core/committer/version_naming.html)
 */
public final class WechatSdk {

    /**
     * 获取 WechatSdk 的版本号，版本号的命名规范
     *
     * <pre>
     * [大版本号].[质量号].[发布流水号]
     * </pre>
     *
     * @return 项目的版本号
     */
    public static String version() {
        return String.format("%d.%s.%d", majorVersion(), releaseLevel(), minorVersion());
    }

    private static int majorVersion() {
        return 1;
    }

    private static int minorVersion() {
        return 0;
    }

    private static String releaseLevel() {
        // a: 内部测试品质, b: 公测品质, r: 最终发布版
        return "b";
    }
}
