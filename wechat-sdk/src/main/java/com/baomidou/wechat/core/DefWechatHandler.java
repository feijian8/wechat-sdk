package com.baomidou.wechat.core;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;

import com.baomidou.wechat.vo.event.BasicEvent;
import com.baomidou.wechat.vo.event.LocationEvent;
import com.baomidou.wechat.vo.event.MenuEvent;
import com.baomidou.wechat.vo.event.ScanCodeEvent;
import com.baomidou.wechat.vo.event.ScanEvent;
import com.baomidou.wechat.vo.event.SendLocationInfoEvent;
import com.baomidou.wechat.vo.event.SendPhotosEvent;
import com.baomidou.wechat.vo.message.Article;
import com.baomidou.wechat.vo.message.BasicMsg;
import com.baomidou.wechat.vo.message.ImageMsg;
import com.baomidou.wechat.vo.message.LinkMsg;
import com.baomidou.wechat.vo.message.LocationMsg;
import com.baomidou.wechat.vo.message.NewsMsg;
import com.baomidou.wechat.vo.message.TextMsg;
import com.baomidou.wechat.vo.message.VideoMsg;
import com.baomidou.wechat.vo.message.VoiceMsg;
import com.baomidou.wechat.vo.push.SentAllJobEvent;
import com.baomidou.wechat.vo.push.SentTmlJobEvent;

/**
 * 微信消息,事件处理器(实际生产中需要重写)
 */
public class DefWechatHandler implements WechatHandler {

	public BasicMsg defMsg(BasicMsg bm) {
		TextMsg tm = new TextMsg(bm);
		tm.setContent(bm.getMsgType());
		return tm;
	}

	public BasicMsg defEvent(BasicEvent be) {
		TextMsg tm = new TextMsg(be);
		tm.setContent(StringUtils.join(new String[] { be.getEvent(), be.getEventKey() }, "\n"));
		return tm;
	}

	public BasicMsg text(TextMsg tm) {
		return tm;
	}

	public BasicMsg image(ImageMsg im) {
		return im;
	}

	public BasicMsg voice(VoiceMsg vm) {
		TextMsg tm = new TextMsg(vm);
		tm.setContent(StringUtils.join(new String[] { vm.getMediaId(), vm.getFormat(), vm.getRecognition() }, "\n"));
		return tm;
	}

	public BasicMsg video(VideoMsg vim) {
		TextMsg tm = new TextMsg(vim);
		tm.setContent(StringUtils.join(new String[] { vim.getMsgType(), vim.getMediaId(), vim.getThumbMediaId() }, "\n"));
		return tm;
	}

	public BasicMsg shortVideo(VideoMsg vim) {
		TextMsg tm = new TextMsg(vim);
		tm.setContent(StringUtils.join(new String[] { vim.getMsgType(), vim.getMediaId(), vim.getThumbMediaId() }, "\n"));
		return tm;
	}

	public BasicMsg location(LocationMsg lm) {
		TextMsg tm = new TextMsg(lm);
		tm.setContent(StringUtils.join(new String[] { lm.getX(), lm.getY(), String.valueOf(lm.getScale()), lm.getLabel() }, "\n"));
		return tm;
	}

	@Override
	public BasicMsg link(LinkMsg lm) {
		NewsMsg news = new NewsMsg(lm);
		Article art = new Article();
		art.setTitle(lm.getTitle());
		art.setDescription(lm.getDescription());
		art.setPicUrl("http://mp.weixin.qq.com/debug/zh_CN/htmledition/images/bg/bg_logo1f2fc8.png");
		art.setUrl(lm.getUrl());
		news.setArticles(Arrays.asList(art));
		return news;
	}

	@Override
	public BasicMsg eClick(MenuEvent me) {
		TextMsg tm = new TextMsg(me);
		tm.setContent(me.getEventKey());
		return tm;
	}

	@Override
	public void eView(MenuEvent me) {
	}

	@Override
	public BasicMsg eSub(BasicEvent be) {
		TextMsg tm = new TextMsg(be);
		tm.setContent("Welcom, wechat develop with use mpsdk4j!");
		return tm;
	}

	@Override
	public void eUnSub(BasicEvent be) {
	}

	@Override
	public BasicMsg eScan(ScanEvent se) {
		TextMsg tm = new TextMsg(se);
		tm.setContent(se.getEventKey() + se.getTicket());
		return tm;
	}

	@Override
	public void eLocation(LocationEvent le) {
	}

	@Override
	public BasicMsg eScanCodePush(ScanCodeEvent sce) {
		TextMsg tm = new TextMsg(sce);
		tm.setContent(StringUtils.join(new String[] { sce.getEventKey(), sce.getScanType(), sce.getScanResult() }, "\n"));
		return tm;
	}

	@Override
	public BasicMsg eScanCodeWait(ScanCodeEvent sce) {
		return this.eScanCodePush(sce);
	}

	@Override
	public BasicMsg ePicSysPhoto(SendPhotosEvent spe) {
		TextMsg tm = new TextMsg(spe);
		tm.setContent(StringUtils.join(new String[] { spe.getEventKey(),
				String.valueOf(spe.getSendPicsInfo().getCount()), String.valueOf(spe.getSendPicsInfo().getPicList()),
				String.valueOf(spe.getSendPicsInfo().getPicList().get(0).getPicMd5Sum()) }, "\n"));
		return tm;
	}

	@Override
	public BasicMsg ePicPhotoOrAlbum(SendPhotosEvent spe) {
		return this.ePicSysPhoto(spe);
	}

	@Override
	public BasicMsg ePicWeixin(SendPhotosEvent spe) {
		return this.ePicSysPhoto(spe);
	}

	@Override
	public BasicMsg eLocationSelect(SendLocationInfoEvent slie) {
		TextMsg tm = new TextMsg(slie);
		tm.setContent(StringUtils.join(new String[] { slie.getLocationX(), slie.getLocationY(), slie.getLabel(),
				String.valueOf(slie.getScale()), slie.getPoiname() }, "\n"));
		return tm;
	}

	@Override
	public void eSentTmplJobFinish(SentTmlJobEvent stje) {
		
	}

	@Override
	public void eSentAllJobFinish(SentAllJobEvent saje) {
		
	}

}
