package com.baomidou.wechat.vo.api;

import com.baomidou.wechat.vo.ApiResult;

/**
 * 短网址
 */
public class ShortUrl extends ApiResult {
	private String short_url;

	public String getShort_url() {
		return short_url;
	}

	public void setShort_url(String short_url) {
		this.short_url = short_url;
	}

}
