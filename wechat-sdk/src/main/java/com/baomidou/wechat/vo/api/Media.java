package com.baomidou.wechat.vo.api;

import com.baomidou.wechat.vo.ApiResult;

/**
 * 多媒体文件
 */
public class Media extends ApiResult {
	/**
	 * 媒体文件类型,分别有
	 * 
	 * <pre/>
	 * 图片(image)
	 * <p/>
	 * 语音(voice)
	 * <p/>
	 * 视频(video)和
	 * <p/>
	 * 缩略图(thumb,主要用于视频与音乐格式的缩略图)
	 */
	private String type;
	/**
	 * 媒体文件上传后，获取时的唯一标识
	 */
	private String media_id;
	/**
	 * 媒体文件上传时间戳
	 */
	private long created_at;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public long getCreated_at() {
		return created_at;
	}

	public void setCreated_at(long created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		return "Media [type=" + type + ", media_id=" + media_id + ", created_at=" + created_at + "]";
	}
}
