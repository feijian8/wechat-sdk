package com.baomidou.wechat.vo.api;

import java.util.List;

import com.baomidou.wechat.vo.ApiResult;

/**
 * 微信服务器IP
 */
public class ServerIp extends ApiResult {
	private List<String> ip_list;

	public List<String> getIp_list() {
		return ip_list;
	}

	public void setIp_list(List<String> ip_list) {
		this.ip_list = ip_list;
	}
}
