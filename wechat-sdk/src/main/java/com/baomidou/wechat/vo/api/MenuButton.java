package com.baomidou.wechat.vo.api;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSON;

/**
 * 自定义菜单按钮
 */
public class MenuButton {
	/**
	 * 菜单列表
	 */
	private List<Menu> button;
	/**
	 * 返回json menu字符串
	 */
	private String menu;

	public List<Menu> getButton() {
		return button;
	}

	public void setButton(List<Menu> button) {
		this.button = button;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
		if (StringUtils.isNotBlank(menu)) {
			MenuButton mb = JSON.parseObject(menu, MenuButton.class);
			if (mb != null) {
				setButton(mb.getButton());
			}
		}
	}

}
