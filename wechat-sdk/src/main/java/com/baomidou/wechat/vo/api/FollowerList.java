package com.baomidou.wechat.vo.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.wechat.vo.ApiResult;

/**
 * 关注者列表
 */
public class FollowerList extends ApiResult {
	/**
	 * 关注该公众账号的总用户数
	 */
	private int total;
	/**
	 * 拉取的OPENID个数,最大值为10000
	 */
	private int count;
	/**
	 * 列表数据,OPENID的列表
	 */
	private List<String> openIds;
	/**
	 * 拉取列表的后一个用户的OPENID
	 */
	private String next_openid;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * 解析 data 数据
	 */
	public void setData(String data) {
		if (StringUtils.isNotBlank(data)) {
			JSONArray ja = JSON.parseObject(data).getJSONArray("openid");
			if (ja != null) {
				int size = ja.size();
				if (size >= 1) {
					openIds = new ArrayList<String>();
					for (int i = 0; i < size; i++) {
						openIds.add(ja.getString(i));
					}
				}
			}
		}
	}

	public List<String> getOpenIds() {
		return openIds;
	}

	public String getNext_openid() {
		return next_openid;
	}

	public void setNext_openid(String next_openid) {
		this.next_openid = next_openid;
	}

	@Override
	public String toString() {
		return "FollowList [total=" + total + ", count=" + count + ", openIds=" + openIds + ", next_openid="
				+ next_openid + "]";
	}
}
