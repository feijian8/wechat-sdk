package com.baomidou.wechat.common.enums;

/**
 * 多媒体文件类型
 */
public enum MediaType {
	/* 图片 */
	IMAGE("image", "图片: 1M,支持JPG格式"),

	/* 语音 */
	VOICE("voice", "语音:2M,播放长度不超过60s,支持AMR,MP3格式"),

	/* 视频 */
	VIDEO("video", "视频:10MB,支持MP4格式"),

	/* 缩略图 */
	THUMB("thumb", "缩略图:64KB,支持JPG格式");

	private final String key;
	private final String desc;

	private MediaType(String key, String desc) {
		this.key = key;
		this.desc = desc;
	}

	public String key() {
		return this.key;
	}

	public String desc() {
		return this.desc;
	}
}
