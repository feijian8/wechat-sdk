package com.baomidou.wechat.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

import com.baomidou.wechat.exception.WechatException;

/**
 * HTTP 请求工具类
 */
public class HttpUtil {
	private static final String DEFAULT_CHARSET = "UTF-8";

	/**
	 * HTTP GET 请求
	 * 
	 * @param url
	 *            请求地址
	 * @return 返回HTTP请求结果 {@link Response}
	 * 
	 * @throws WechatException
	 */
	public static Response get(String url) throws WechatException {
		try {
			return getConnection(url).method(Method.GET).execute();
		} catch (Exception e) {
			throw new WechatException(e);
		}
	}

	/**
	 * HTTP POST 请求
	 * 
	 * @param url
	 *            请求地址
	 * @param data
	 *            请求参数
	 * @return 返回HTTP请求结果 {@link Response}
	 * 
	 * @throws WechatException
	 */
	public static Response post(String url, Map<String, String> data) throws WechatException {
		try {
			return getConnection(url).data(data).method(Method.POST).execute();
		} catch (Exception e) {
			throw new WechatException(e);
		}
	}

	/**
	 * HTTP POST 请求
	 * 
	 * @param url
	 *            请求地址
	 * @param json
	 *            请求 json
	 * @return WechatException
	 */
	public static String postJson(String url, String json) throws WechatException {
		try {
			byte[] xmlData = json.getBytes();
			URLConnection urlCon = new URL(url).openConnection();
			urlCon.setDoOutput(true);
			urlCon.setDoInput(true);
			urlCon.setUseCaches(false);
			urlCon.setRequestProperty("Content-Type", "text/xml");
			urlCon.setRequestProperty("Content-length", String.valueOf(xmlData.length));
			DataOutputStream printout = new DataOutputStream(urlCon.getOutputStream());
			printout.write(xmlData);
			printout.flush();
			printout.close();
			byte[] bts = StreamUtil.toBytes(urlCon.getInputStream());
			return new String(bts, "UTF-8");
		} catch (Exception e) {
			throw new WechatException(e);
		}
	}

	/**
	 * 获得请求连接
	 * 
	 * @param url
	 *            请求地址
	 * @return 返回HTTP请求连接 {@link Connection}
	 */
	public static Connection getConnection(String url) {
		return Jsoup.connect(url).userAgent("Mozilla").ignoreContentType(true);
	}

	/**
	 * 初始化http请求参数
	 * 
	 * @param url
	 * @param method
	 * @param headers
	 * @return
	 * @throws IOException
	 */
	private static HttpURLConnection initHttp(String url, String method, Map<String, String> headers)
			throws IOException {
		URL _url = new URL(url);
		HttpURLConnection http = (HttpURLConnection) _url.openConnection();
		/* 连接超时 */
		http.setConnectTimeout(25000);

		/* 读取超时 --服务器响应比较慢，增大时间 */
		http.setReadTimeout(25000);
		http.setRequestMethod(method);
		http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		http.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36");
		if (null != headers && !headers.isEmpty()) {
			for (Entry<String, String> entry : headers.entrySet()) {
				http.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
		http.setDoOutput(true);
		http.setDoInput(true);
		http.connect();
		return http;
	}

	/**
	 * 检测是否https
	 * 
	 * @param url
	 */
	protected static boolean isHttps(String url) {
		return url.startsWith("https");
	}

	/**
	 * 初始化https请求参数
	 * 
	 * @param url
	 * @param method
	 * @return
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws KeyManagementException
	 */
	protected static HttpsURLConnection initHttps(String url, String method, Map<String, String> headers)
			throws IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyManagementException {
		TrustManager[] tm = { new MyX509TrustManager() };
		SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
		sslContext.init(null, tm, new java.security.SecureRandom());
		/**
		 * 从上述SSLContext对象中得到SSLSocketFactory对象
		 */
		SSLSocketFactory ssf = sslContext.getSocketFactory();
		URL _url = new URL(url);
		HttpsURLConnection http = (HttpsURLConnection) _url.openConnection();
		/* 连接超时 */
		http.setConnectTimeout(25000);
		/* 读取超时 --服务器响应比较慢，增大时间 */
		http.setReadTimeout(25000);
		http.setRequestMethod(method);
		http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		http.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36");
		if (null != headers && !headers.isEmpty()) {
			for (Entry<String, String> entry : headers.entrySet()) {
				http.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
		http.setSSLSocketFactory(ssf);
		http.setDoOutput(true);
		http.setDoInput(true);
		http.connect();
		return http;
	}

	/**
	 * 上传媒体文件
	 * 
	 * @param url
	 * @param params
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws KeyManagementException
	 */
	public static String upload(String url, File file)
			throws IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyManagementException {
		/* 定义数据分隔线 */
		String BOUNDARY = "----WebKitFormBoundaryiDGnV9zdZA1eM1yL";
		StringBuffer bufferRes = null;
		URL urlGet = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) urlGet.openConnection();
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setUseCaches(false);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("connection", "Keep-Alive");
		conn.setRequestProperty("user-agent",
				"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36");
		conn.setRequestProperty("Charsert", "UTF-8");
		conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);

		OutputStream out = new DataOutputStream(conn.getOutputStream());
		byte[] end_data = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();// 定义最后数据分隔线
		StringBuilder sb = new StringBuilder();
		sb.append("--");
		sb.append(BOUNDARY);
		sb.append("\r\n");
		sb.append("Content-Disposition: form-data;name=\"media\";filename=\"" + file.getName() + "\"\r\n");
		sb.append("Content-Type:application/octet-stream\r\n\r\n");
		byte[] data = sb.toString().getBytes();
		out.write(data);
		DataInputStream fs = new DataInputStream(new FileInputStream(file));
		int bytes = 0;
		byte[] bufferOut = new byte[1024];
		while ((bytes = fs.read(bufferOut)) != -1) {
			out.write(bufferOut, 0, bytes);
		}

		/* 多个文件时，二个文件之间加入这个 */
		out.write("\r\n".getBytes());
		fs.close();
		out.write(end_data);
		out.flush();
		out.close();

		/* 定义BufferedReader输入流来读取URL的响应 */
		InputStream in = conn.getInputStream();
		BufferedReader read = new BufferedReader(new InputStreamReader(in, DEFAULT_CHARSET));
		String valueString = null;
		bufferRes = new StringBuffer();
		while ((valueString = read.readLine()) != null) {
			bufferRes.append(valueString);
		}
		in.close();
		if (conn != null) {
			/* 关闭连接 */
			conn.disconnect();
		}
		return bufferRes.toString();
	}

	/**
	 * 下载资源
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static Attachment download(String url) throws IOException {
		Attachment att = new Attachment();
		HttpURLConnection conn = initHttp(url, "GET", null);
		if (conn.getContentType().equalsIgnoreCase("text/plain")) {
			/* 定义BufferedReader输入流来读取URL的响应 */
			InputStream in = conn.getInputStream();
			BufferedReader read = new BufferedReader(new InputStreamReader(in, DEFAULT_CHARSET));
			String valueString = null;
			StringBuffer bufferRes = new StringBuffer();
			while ((valueString = read.readLine()) != null) {
				bufferRes.append(valueString);
			}
			in.close();
			att.setError(bufferRes.toString());
		} else {
			BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
			String ds = conn.getHeaderField("Content-disposition");
			String fullName = ds.substring(ds.indexOf("filename=\"") + 10, ds.length() - 1);
			String relName = fullName.substring(0, fullName.lastIndexOf("."));
			String suffix = fullName.substring(relName.length() + 1);

			att.setFullName(fullName);
			att.setFileName(relName);
			att.setSuffix(suffix);
			att.setContentLength(conn.getHeaderField("Content-Length"));
			att.setContentType(conn.getHeaderField("Content-Type"));

			att.setFileStream(bis);
		}
		return att;
	}

}

/**
 * 证书管理
 */
class MyX509TrustManager implements X509TrustManager {

	public X509Certificate[] getAcceptedIssuers() {
		return null;
	}

	@Override
	public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	}

	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	}
}
