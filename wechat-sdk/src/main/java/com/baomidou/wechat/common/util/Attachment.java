package com.baomidou.wechat.common.util;

import java.io.BufferedInputStream;

/**
 * 下载文件对象
 */
public class Attachment {
	/*
	 * 文件名
	 */
	private String fileName;
	/*
	 * 文件全名
	 */
	private String fullName;
	/*
	 * 文件后缀
	 */
	private String suffix;
	/*
	 * 返回内容长度
	 */
	private String contentLength;
	/*
	 * 返回内容类型
	 */
	private String contentType;
	/*
	 * 文件流
	 */
	private BufferedInputStream fileStream;
	/*
	 * 返回错误
	 */
	private String error;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentLength() {
		return contentLength;
	}

	public void setContentLength(String contentLength) {
		this.contentLength = contentLength;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public BufferedInputStream getFileStream() {
		return fileStream;
	}

	public void setFileStream(BufferedInputStream fileStream) {
		this.fileStream = fileStream;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getSuffix() {
		return suffix;
	}

	@Override
	public String toString() {
		return "Attachment [fileName=" + fileName + ", fullName=" + fullName + ", suffix=" + suffix + ", contentLength="
				+ contentLength + ", contentType=" + contentType + ", fileStream=" + fileStream + ", error=" + error
				+ "]";
	}
}
